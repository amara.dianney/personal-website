import React from "react";

function Footer() {
  return (
    <footer className="footer has-background-link">
      <div className="content has-text-centered has-text-white">
        <p>
          Dévéloppé avec <i className="fas fa-heart"/> par{" "}
          <a href="https://gitlab.com/amara.dianney" className="has-text-white">
            <strong>Amara Bamba</strong>
          </a>
        </p>
      </div>
    </footer>
  );
}

export default Footer;
